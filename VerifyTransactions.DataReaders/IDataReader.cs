﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using VerifyTransactions.DataReaders.Models;

namespace VerifyTransactions.DataReaders
{
    public interface IDataReader
    {
        Task<Dictionary<string, TransactionItem>> ReadTransactionsAsync(ReadOptions options, CancellationToken cancellationToken = default);
    }
}
