﻿namespace VerifyTransactions.DataReaders.Exceptions
{
    public class RequaredColumnNotFoundException : ReaderException
    {
        public RequaredColumnNotFoundException()
            : base("Не найден один или несколько необходимых столбцов с данными ")
        {

        }
    }
}
