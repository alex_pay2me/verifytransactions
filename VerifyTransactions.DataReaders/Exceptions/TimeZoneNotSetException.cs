﻿namespace VerifyTransactions.DataReaders.Exceptions
{
    public class TimeZoneNotSetException : ReaderException
    {
        public TimeZoneNotSetException() 
            : base("Не задан часовой пояс меток времени")
        {

        }
    }
}
