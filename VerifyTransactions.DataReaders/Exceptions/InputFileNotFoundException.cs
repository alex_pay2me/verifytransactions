﻿using System;

namespace VerifyTransactions.DataReaders.Exceptions
{
    public class InputFileNotFoundException : ReaderException
    {
        public string FilePath { get; set; }

        public InputFileNotFoundException(string filePath)
            : base("Не удалось найти файл по указанному пути")
        {
            FilePath = filePath;
        }
    }
}
