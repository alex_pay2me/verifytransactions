﻿namespace VerifyTransactions.DataReaders.Exceptions
{
    public class NoSheetsFoundException : ReaderException
    {
        public NoSheetsFoundException() 
            : base("В файле не найдено листов с данными")
        {

        }
    }
}
