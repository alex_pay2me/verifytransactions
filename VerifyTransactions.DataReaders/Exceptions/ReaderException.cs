﻿using System;

namespace VerifyTransactions.DataReaders.Exceptions
{
    public abstract class ReaderException : Exception
    {
        protected ReaderException(string message) : base(message)
        {

        }
    }
}
