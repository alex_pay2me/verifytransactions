﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using OfficeOpenXml;
using VerifyTransactions.DataReaders.Exceptions;
using VerifyTransactions.DataReaders.Models;

namespace VerifyTransactions.DataReaders.Providers
{
    internal sealed class JysanExcelDataReader : IDataReader
    {
        internal JysanExcelDataReader()
        { }

        const string TimeStampColumn = "Дата операции";
        const string PANMaskColumn = "Номер карты";
        const string RefNumberColumn = "Ref number";
        const string AmountColumn = "Сумма";

        /// <summary>
        /// Индексы столбцов с данными в файле
        /// </summary>
        private class DataIndices
        {
            public int StartRow { get; set; } = -1;

            public int TimeStamp { get; set; } = -1;

            public int PANMask { get; set; } = -1;

            public int RefNumber { get; set; } = -1;

            public int Amount { get; set; } = -1;

            public bool AllColumnsFound => TimeStamp != -1 && PANMask != -1 && RefNumber != -1 && Amount != -1;
        }

        public async Task<Dictionary<string, TransactionItem>> ReadTransactionsAsync(ReadOptions options, CancellationToken cancellationToken = default)
        {
            if (!File.Exists(options.FilePath))
                throw new InputFileNotFoundException(options.FilePath);
            if (options.TimeZone == null)
                throw new TimeZoneNotSetException();

            return await Task.Run(() =>
            {
                Dictionary<string, TransactionItem> result = new();

                using ExcelPackage package = new(new FileInfo(options.FilePath));
                using ExcelWorksheet sheet = package.Workbook.Worksheets?.FirstOrDefault()
                    ?? throw new NoSheetsFoundException();

                DataIndices indices = FindColumnIndices(sheet);

                if (!indices.AllColumnsFound)
                    throw new RequaredColumnNotFoundException();

                for (int row = indices.StartRow + 1; row <= sheet.Dimension.Rows; row++)
                {
                    string date = sheet.Cells[row, indices.TimeStamp].Value?.ToString().Trim();
                    string rrn = sheet.Cells[row, indices.RefNumber].Value?.ToString().Trim();
                    string pan = sheet.Cells[row, indices.PANMask].Value?.ToString().Trim();
                    string amount = sheet.Cells[row, indices.Amount].Value?.ToString().Trim();

                    if (string.IsNullOrEmpty(date) ||
                        string.IsNullOrEmpty(rrn) ||
                        string.IsNullOrEmpty(pan) ||
                        string.IsNullOrEmpty(amount))
                        continue;

                    if (!decimal.TryParse(amount, NumberStyles.Any, CultureInfo.InvariantCulture, out decimal amountValue))
                        continue;

                    if (result.ContainsKey(rrn))
                    {
                        result[rrn].Status = amountValue > 0 ? Status.Debited : Status.Refunded;
                        continue;
                    }

                    string offset = GetTimeZoneOffset(options.TimeZone);

                    if (!DateTime.TryParseExact(date + " " + offset, "dd.MM.yyyy HH:mm:ss zzz", null, DateTimeStyles.AssumeLocal, out DateTime timeStamp))
                        continue;

                    TransactionItem transaction = new()
                    {
                        PANMask = pan,
                        RefNumber = rrn,
                        Amount = (long)(amountValue * 100),
                        TimeStamp = timeStamp.ToUniversalTime(),
                        Status = amountValue > 0 ? Status.Debited : Status.Refunded
                    };

                    result[rrn] = transaction;
                }

                return result;
            });            
        }

        private static string GetTimeZoneOffset(TimeZoneInfo zone)
        {
            string sign = zone.BaseUtcOffset.Ticks < 0
                ? "-"
                : "+";

            string hours = zone.BaseUtcOffset.Hours.ToString();
            if (hours.Length == 1)
                hours = $"0{hours}";

            string min = zone.BaseUtcOffset.Minutes.ToString();
            if (min.Length == 1)
                min = $"0{min}";

            return $"{sign}{hours}:{min}";
        }

        /// <summary>
        /// Поиск столбцов с данными в эксель файле
        /// </summary>
        /// <param name="sheet"></param>
        /// <returns></returns>
        private static DataIndices FindColumnIndices(ExcelWorksheet sheet)
        {
            DataIndices indices = new();

            for (int row = 1; row <= sheet.Dimension.Rows; row++)
            {
                for (int col = 1; col <= sheet.Dimension.Columns; col++)
                {
                    string value = sheet.Cells[row, col].Value?.ToString().Trim();
                    if (string.IsNullOrEmpty(value))
                        continue;

                    if (value.Equals(TimeStampColumn, StringComparison.OrdinalIgnoreCase))
                    {
                        indices.StartRow = row;
                        indices.TimeStamp = col;
                    }
                    else if (value.Equals(PANMaskColumn, StringComparison.OrdinalIgnoreCase))
                    {
                        indices.PANMask = col;
}
                    else if (value.Equals(RefNumberColumn, StringComparison.OrdinalIgnoreCase))
                    {
                        indices.RefNumber = col;
                    }
                    else if (value.Equals(AmountColumn, StringComparison.OrdinalIgnoreCase))
                    {
                        indices.Amount = col;
                    }

                    if (indices.AllColumnsFound)
                        return indices;
                }
            }

            return indices;
        }
    }
}
