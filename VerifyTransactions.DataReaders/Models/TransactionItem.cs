﻿using System;

namespace VerifyTransactions.DataReaders.Models
{
    /// <summary>
    /// Данные о транзакции
    /// </summary>
    public class TransactionItem
    {
        /// <summary>
        /// Дата операции
        /// </summary>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Маска карты
        /// </summary>
        public string PANMask { get; set; }

        /// <summary>
        /// Ref number (RRN)
        /// </summary>
        public string RefNumber { get; set; }

        /// <summary>
        /// Сумма транзакции
        /// </summary>
        public long Amount { get; set; }

        /// <summary>
        /// Статус транзакции
        /// </summary>
        public Status Status { get; set; }
    }
}
