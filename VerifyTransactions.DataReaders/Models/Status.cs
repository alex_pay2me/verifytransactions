﻿namespace VerifyTransactions.DataReaders.Models
{
    /// <summary>
    /// Статус транзакции
    /// </summary>
    public enum Status
    {
        /// <summary>
        /// Отклонен
        /// </summary>
        Rejected = -1,
        /// <summary>
        /// Списан
        /// </summary>
        Debited = 8,
        /// <summary>
        /// Возвращен
        /// </summary>
        Refunded = 10,
        /// <summary>
        /// Выплачен
        /// </summary>
        Paid = 16
    }
}
