﻿using System;

namespace VerifyTransactions.DataReaders.Models
{
    public class ReadOptions
    {
        /// <summary>
        /// Смещение меток времени относительно UTC
        /// </summary>
        public TimeZoneInfo TimeZone { get; set; }

        /// <summary>
        /// Путь к файлу с данными для импорта
        /// </summary>
        public string FilePath { get; set; }
    }
}
