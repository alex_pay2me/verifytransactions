﻿using System;
using VerifyTransactions.DataReaders.Models;
using VerifyTransactions.DataReaders.Providers;

namespace VerifyTransactions.DataReaders
{
    public class DataReadersFactory
    {
        /// <summary>
        /// Уникальное имя банка Jysan
        /// </summary>
        const string JysanBank = "JysanBank";
        /// <summary>
        /// Уникальное имя банка AlfaBank (KZ)
        /// </summary>
        const string AlfabankKZ = "AlfabankKZ";

        public IDataReader CreateReader(string processingName, DataFormat format)
        {
            if (processingName == JysanBank && format == DataFormat.XLSX)
                return new JysanExcelDataReader();

            if (processingName == AlfabankKZ && format == DataFormat.XLSX)
                return new AlfaKZExcelDataReader();

            throw new Exception($"Не реализован функционал чтения данных в формате {DataFormat.XLSX} для предприятия {processingName}");
        }
    }
}
