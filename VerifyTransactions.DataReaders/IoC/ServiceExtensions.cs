﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace VerifyTransactions.DataReaders.IoC
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddDataReaders(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<DataReadersFactory>();
             
            return services;
        }
    }
}
