﻿using System;
using Newtonsoft.Json;

namespace VerifyTransactions.GUI.Helpers
{
    internal static class JsonSerializer
    {
        /// <summary>
        /// Настройки сериализации
        /// </summary>
        static readonly JsonSerializerSettings _settings = new()
        {
            TypeNameHandling = TypeNameHandling.Auto,
            NullValueHandling = NullValueHandling.Ignore
        };

        static JsonSerializer()
        {

        }

        /// <summary>
        /// Сериализуем объект в строку JSON
        /// </summary>
        /// <typeparam name="T">Тип сериализуемого объекта</typeparam>
        /// <param name="obj">Сериализуемый объект</param>
        /// <returns>Строка JSON</returns>
        internal static string WriteFromObject<T>(T obj, bool formatting = true)
        {
            return obj == null
                ? throw new ArgumentNullException("Объект не инициализирован.")
                : JsonConvert.SerializeObject(obj, formatting ? Formatting.Indented : Formatting.None, _settings);
        }

        /// <summary>
        /// Десериализуем объект из строки JSON
        /// </summary>
        /// <typeparam name="T">Тип десериализуемого объекта</typeparam>
        /// <param name="json">Строка JSON</param>
        /// <returns>Десериализованный объект</returns>
        internal static T ReadToObject<T>(string json)
        {
            if (json == null)
                throw new ArgumentNullException("Строка JSON не инициализирована.");

            return json == string.Empty
                ? throw new ArgumentException("Пустая строка JSON.")
                : JsonConvert.DeserializeObject<T>(json, _settings);
        }
    }
}
