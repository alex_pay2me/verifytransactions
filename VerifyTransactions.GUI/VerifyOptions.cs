﻿using System;
using Connector.Map.Api.Models;

namespace VerifyTransactions.GUI
{
    internal class VerifyOptions
    {
        public Processing Processing { get; set; }

        public Merchant Merchant { get; set; }

        public Terminal Terminal { get; set; }

        public TimeZoneInfo TimeZone { get; set; }

        public DateTime DateStart { get; set; }

        public DateTime DateEnd { get; set; }

        public string[] ExcelFiles { get; set; }
    }
}
