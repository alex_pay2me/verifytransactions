﻿using System;

namespace VerifyTransactions.GUI
{
    /// <summary>
    /// Различия в транзакциях
    /// </summary>    
    internal enum Differences
    {
        /// <summary>
        /// Нет различий
        /// </summary>
        None,

        /// <summary>
        /// Нет в МАР
        /// </summary>
        NotFoundInMAP,

        /// <summary>
        /// Нет в выписке
        /// </summary>
        NotFoundInBankStatement,

        /// <summary>
        /// Расхождение по сумме
        /// </summary>
        DifferentAmount,

        /// <summary>
        /// Расхождение по статусу
        /// </summary>
        DifferentState,

        /// <summary>
        /// Не попал в интервал
        /// </summary>
        NotInRange
    }
}
