﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Connector.Map.Api.Models;
using Connector.Map.Api.Services;
using VerifyTransactions.GUI.ViewModels;

namespace VerifyTransactions.GUI.Controls
{
    public partial class ActualizeDataControl : UserControl
    {
        private ITheMapApiClient _mapApiClient;
        private List<Processing> _processings;
        private Dictionary<long, List<Merchant>> _processingMerchantsMap;
        private Dictionary<long, List<Terminal>> _merchantTerminalsMap;

        private readonly List<MerchantOrderStateView> _ordersStateDataSource = new();

        public ActualizeDataControl()
        {
            InitializeComponent();
        }

        internal void Initialize(ITheMapApiClient mapApiClient,
            List<Processing> processings, 
            Dictionary<long, List<Merchant>> processingMerchantsMap, 
            Dictionary<long, List<Terminal>> merchantTerminalsMap)
        {
            _mapApiClient = mapApiClient;
            _processings = processings;
            _processingMerchantsMap = processingMerchantsMap;
            _merchantTerminalsMap = merchantTerminalsMap;

            cmbProcessing.DataSource = _processings;
            if (_processings.Count > 0)
                cmbProcessing.SelectedIndex = 0;
        }

        private void OnProcessing_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbProcessing.SelectedItem is not Processing processing)
                return;
            if (!_processingMerchantsMap.TryGetValue(processing.Id, out var merchantsList))
                return;

            cmbMerchant.DataSource = merchantsList;
            if (merchantsList.Count > 0)
                cmbMerchant.SelectedIndex = 0;
        }

        private void OnMerchant_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbProcessing.SelectedItem is not Processing processing)
                return;
            if (cmbMerchant.SelectedItem is not Merchant merchant)
                return;

            var terminals = _merchantTerminalsMap[merchant.Id]
                .Where(t => t.ProcessingId == processing.Id).ToList();

            cmbTerminal.DataSource = terminals;
            if (terminals.Count > 0)
                cmbTerminal.SelectedIndex = 0;
        }

        private async void OnActualizeMerchantOrders_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbTerminal.SelectedItem is not Terminal terminal)
                    throw new Exception("Не выбран терминал");

                btnActualizeMerchantOrders.Enabled = false;

                List<string> merchantOrders = GerMerchantOrderIds();
                if (merchantOrders.Count == 0)
                {
                    throw new Exception("Введите номера заказов мерчанта. Каждый номер с новой строки!");
                }

                SetLoadingVisibility(true);

                List<ActualizeOrderResponce> merchantOrdersStates = await GetOrderStates(merchantOrders, terminal.Name);

                _ordersStateDataSource.Clear();
                _ordersStateDataSource.AddRange(merchantOrdersStates.Select(x => new MerchantOrderStateView(x)));

                RefreshDataSource();
            }
            catch (Exception ex)
            {
                SetLoadingVisibility(false);
                MessageBox.Show(this, ex.Message);
            }
            finally
            {
                btnActualizeMerchantOrders.Enabled = true;
                SetLoadingVisibility(false);
            }
        }

        private async Task<List<ActualizeOrderResponce>> GetOrderStates(List<string> merchantOrders, string terminalName)
        {
            Progress progress = new(merchantOrders.Count);

            List<ActualizeOrderResponce> result = new(merchantOrders.Count);

            ActualizeOrderRequest request = new()
            {
                Key = terminalName,
                IsOverload = true
            };

            foreach (var orderId in merchantOrders)
            {
                request.MerchantOrderId = orderId;

                ActualizeOrderResponce responce = await _mapApiClient.GetMerchantOrderStateAsync(request);

                responce.MerchantOrderId = orderId;

                result.Add(responce);

                progress.CountDone++;
                lbPercetage.Text = $"{progress.Percentage:N1}%...";
            }

            return result;
        }

        private void RefreshDataSource()
        {
            if (InvokeRequired)
                Invoke(new Action(RefreshDataSource));
            else
            {
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = _ordersStateDataSource;
            }
        }

        /// <summary>
        /// Установить видимость панели прогресса
        /// </summary>
        /// <param name="visible">Видимость панели</param>
        void SetLoadingVisibility(bool visible)
        {
            if (InvokeRequired)
                BeginInvoke(new Action(() => { SetLoadingVisibility(visible); }));
            else
            {
                lbPercetage.Text = "0%...";
                Point new_location = new(Width / 2 - _reloadingPanel.Width / 2, Height / 2 - _reloadingPanel.Height / 2);
                _reloadingPanel.Location = new_location;
                _reloadingPanel.Visible = visible;
            }
        }

        private List<string> GerMerchantOrderIds()
        {
            return richTextBox1.Lines
                .Where(line => !string.IsNullOrWhiteSpace(line))
                .Select(line => line.Trim())
                .ToList();
        }

        private void ActualizeDataControl_SizeChanged(object sender, EventArgs e)
        {
            if (_reloadingPanel.Visible)
            {
                Point new_location = new(Width / 2 - _reloadingPanel.Width / 2, Height / 2 - _reloadingPanel.Height / 2);
                _reloadingPanel.Location = new_location;
            }
        }
    }
}
