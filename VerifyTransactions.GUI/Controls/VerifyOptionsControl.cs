﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Connector.Map.Api.Models;

namespace VerifyTransactions.GUI.Controls
{
    public partial class VerifyOptionsControl : UserControl
    {
        private Processing _processing;
        private List<Merchant> _merchants;
        private Dictionary<long, List<Terminal>> _merchantTerminals;

        public VerifyOptionsControl()
        {
            InitializeComponent();
        }

        internal void Initialize(Processing processing,
            List<TimeZoneInfo> timeZones,
            List<Merchant> merchants,
            Dictionary<long, List<Terminal>> merchantTerminals,
            DateTime startDate)
        {
            _processing = processing;
            _merchants = merchants;
            _merchantTerminals = merchantTerminals;

            cmbMerchant.DataSource = merchants;
            if (merchants.Count > 0)
                cmbMerchant.SelectedIndex = 0;

            cmbTimeZone.DataSource = timeZones;
            cmbTimeZone.Text = TimeZoneInfo.Local.DisplayName;

            dtStart.Value = startDate;
            dtEnd.Value = startDate.AddDays(1);
        }

        internal VerifyOptions GetSelectedOptions()
        {
            return new VerifyOptions
            { 
                Processing = _processing,
                Merchant = cmbMerchant.SelectedItem as Merchant,
                Terminal = cmbTerminal.SelectedItem as Terminal,
                TimeZone = cmbTimeZone.SelectedItem as TimeZoneInfo,
                DateStart = dtStart.Value,
                DateEnd = dtEnd.Value,
                ExcelFiles = new[] { tbExcelFilePath.Text }
            };
        }

        private void OnMerchant_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbMerchant.SelectedItem is not Merchant merchant)
                return;

            var terminals = _merchantTerminals[merchant.Id]
                .Where(t => t.ProcessingId == _processing.Id).ToList();

            cmbTerminal.DataSource = terminals;
            if (terminals.Count > 0)
                cmbTerminal.SelectedIndex = 0;
        }

        private void OnBrowseButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new()
            {
                Title = "Выберите файл XLSX с суточным реестром от банка",
                Multiselect = false,
                Filter = "Excel файлы *.xlsx|*.xlsx"
            };

            if (open.ShowDialog() != DialogResult.OK)
                return;

            tbExcelFilePath.Text = open.FileName;
        }
    }
}
