﻿
namespace VerifyTransactions.GUI.Controls
{
    partial class ActualizeDataControl
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnActualizeMerchantOrders = new System.Windows.Forms.Button();
            this.cmbTerminal = new System.Windows.Forms.ComboBox();
            this.cmbProcessing = new System.Windows.Forms.ComboBox();
            this.cmbMerchant = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel3 = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.MerchantOrderIdCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SuccessCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RefNumberCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._reloadingPanel = new System.Windows.Forms.Panel();
            this.lbPercetage = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this._reloadingPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.btnActualizeMerchantOrders);
            this.panel1.Controls.Add(this.cmbTerminal);
            this.panel1.Controls.Add(this.cmbProcessing);
            this.panel1.Controls.Add(this.cmbMerchant);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(8, 8);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(10);
            this.panel1.Size = new System.Drawing.Size(765, 171);
            this.panel1.TabIndex = 0;
            // 
            // btnActualizeMerchantOrders
            // 
            this.btnActualizeMerchantOrders.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActualizeMerchantOrders.Location = new System.Drawing.Point(603, 135);
            this.btnActualizeMerchantOrders.Name = "btnActualizeMerchantOrders";
            this.btnActualizeMerchantOrders.Size = new System.Drawing.Size(149, 29);
            this.btnActualizeMerchantOrders.TabIndex = 17;
            this.btnActualizeMerchantOrders.Text = "Актуализировать";
            this.btnActualizeMerchantOrders.UseVisualStyleBackColor = true;
            this.btnActualizeMerchantOrders.Click += new System.EventHandler(this.OnActualizeMerchantOrders_Click);
            // 
            // cmbTerminal
            // 
            this.cmbTerminal.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbTerminal.DisplayMember = "Name";
            this.cmbTerminal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTerminal.FormattingEnabled = true;
            this.cmbTerminal.Location = new System.Drawing.Point(125, 93);
            this.cmbTerminal.Name = "cmbTerminal";
            this.cmbTerminal.Size = new System.Drawing.Size(627, 28);
            this.cmbTerminal.TabIndex = 7;
            // 
            // cmbProcessing
            // 
            this.cmbProcessing.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbProcessing.DisplayMember = "HumanName";
            this.cmbProcessing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProcessing.FormattingEnabled = true;
            this.cmbProcessing.Location = new System.Drawing.Point(125, 10);
            this.cmbProcessing.Name = "cmbProcessing";
            this.cmbProcessing.Size = new System.Drawing.Size(627, 28);
            this.cmbProcessing.TabIndex = 6;
            this.cmbProcessing.SelectedIndexChanged += new System.EventHandler(this.OnProcessing_SelectedIndexChanged);
            // 
            // cmbMerchant
            // 
            this.cmbMerchant.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbMerchant.DisplayMember = "HumanName";
            this.cmbMerchant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMerchant.FormattingEnabled = true;
            this.cmbMerchant.Location = new System.Drawing.Point(125, 50);
            this.cmbMerchant.Name = "cmbMerchant";
            this.cmbMerchant.Size = new System.Drawing.Size(627, 28);
            this.cmbMerchant.TabIndex = 5;
            this.cmbMerchant.SelectedIndexChanged += new System.EventHandler(this.OnMerchant_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "Терминал:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Предприятие:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Банк:";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.splitContainer1);
            this.panel2.Location = new System.Drawing.Point(8, 178);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(765, 386);
            this.panel2.TabIndex = 1;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Cursor = System.Windows.Forms.Cursors.VSplit;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.panel3);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView1);
            this.splitContainer1.Size = new System.Drawing.Size(765, 386);
            this.splitContainer1.SplitterDistance = 191;
            this.splitContainer1.TabIndex = 20;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.richTextBox1);
            this.panel3.Location = new System.Drawing.Point(13, 32);
            this.panel3.Name = "panel3";
            this.panel3.Padding = new System.Windows.Forms.Padding(5);
            this.panel3.Size = new System.Drawing.Size(175, 351);
            this.panel3.TabIndex = 20;
            // 
            // richTextBox1
            // 
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(5, 5);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(163, 339);
            this.richTextBox1.TabIndex = 1;
            this.richTextBox1.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(178, 20);
            this.label4.TabIndex = 19;
            this.label4.Text = "Ид. заказаков мерчанта:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MerchantOrderIdCol,
            this.SuccessCol,
            this.StatusCol,
            this.RefNumberCol});
            this.dataGridView1.EnableHeadersVisualStyles = false;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(564, 380);
            this.dataGridView1.TabIndex = 21;
            // 
            // MerchantOrderIdCol
            // 
            this.MerchantOrderIdCol.DataPropertyName = "MerchantOrderId";
            this.MerchantOrderIdCol.HeaderText = "Ид. заказа мерчата";
            this.MerchantOrderIdCol.Name = "MerchantOrderIdCol";
            this.MerchantOrderIdCol.ReadOnly = true;
            this.MerchantOrderIdCol.Width = 200;
            // 
            // SuccessCol
            // 
            this.SuccessCol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.SuccessCol.DataPropertyName = "Success";
            this.SuccessCol.HeaderText = "Успешно";
            this.SuccessCol.MinimumWidth = 80;
            this.SuccessCol.Name = "SuccessCol";
            this.SuccessCol.ReadOnly = true;
            this.SuccessCol.Width = 80;
            // 
            // StatusCol
            // 
            this.StatusCol.DataPropertyName = "State";
            this.StatusCol.HeaderText = "Статус";
            this.StatusCol.Name = "StatusCol";
            this.StatusCol.ReadOnly = true;
            // 
            // RefNumberCol
            // 
            this.RefNumberCol.DataPropertyName = "RRN";
            this.RefNumberCol.HeaderText = "RRN";
            this.RefNumberCol.Name = "RefNumberCol";
            this.RefNumberCol.ReadOnly = true;
            this.RefNumberCol.Width = 150;
            // 
            // _reloadingPanel
            // 
            this._reloadingPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._reloadingPanel.Controls.Add(this.lbPercetage);
            this._reloadingPanel.Controls.Add(this.label8);
            this._reloadingPanel.Controls.Add(this.pictureBox1);
            this._reloadingPanel.Location = new System.Drawing.Point(279, 250);
            this._reloadingPanel.Name = "_reloadingPanel";
            this._reloadingPanel.Size = new System.Drawing.Size(218, 73);
            this._reloadingPanel.TabIndex = 22;
            this._reloadingPanel.Visible = false;
            // 
            // lbPercetage
            // 
            this.lbPercetage.AutoSize = true;
            this.lbPercetage.Font = new System.Drawing.Font("Segoe UI", 14.28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbPercetage.Location = new System.Drawing.Point(115, 33);
            this.lbPercetage.Name = "lbPercetage";
            this.lbPercetage.Size = new System.Drawing.Size(51, 28);
            this.lbPercetage.TabIndex = 2;
            this.lbPercetage.Text = "0%...";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(73, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(143, 25);
            this.label8.TabIndex = 1;
            this.label8.Text = "Актуализация...";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::VerifyTransactions.GUI.Properties.Resources.preloader;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // ActualizeDataControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._reloadingPanel);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ActualizeDataControl";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Size = new System.Drawing.Size(781, 572);
            this.SizeChanged += new System.EventHandler(this.ActualizeDataControl_SizeChanged);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this._reloadingPanel.ResumeLayout(false);
            this._reloadingPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbProcessing;
        private System.Windows.Forms.ComboBox cmbMerchant;
        private System.Windows.Forms.ComboBox cmbTerminal;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn MerchantOrderIdCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn SuccessCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn RefNumberCol;
        private System.Windows.Forms.Button btnActualizeMerchantOrders;
        private System.Windows.Forms.Panel _reloadingPanel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbPercetage;
    }
}
