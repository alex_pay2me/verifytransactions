﻿namespace VerifyTransactions.GUI
{
    /// <summary>
    /// Счетчик расхождений
    /// </summary>
    internal class DifferenceCounter
    {
        /// <summary>
        /// Всего транзакций из банка
        /// </summary>
        public int TotalFromBank { get; set; }

        /// <summary>
        /// Всего из TheMAP
        /// </summary>
        public int TotalFromMap { get; set; }

        /// <summary>
        /// Всего расхождений
        /// </summary>
        public int Differences { get; set; }

        /// <summary>
        /// Количество транзакций, не найденных в TheMAP за запрошенный период 
        /// </summary>
        public int OutOfDateRangeInMAP { get; set; }

        public override string ToString()
        {
            return $"Сверка окончена.\r\nРасхождений: {Differences}\r\nКоличество транзакций банка: {TotalFromBank}\r\nКоличество транзакций MAP: {TotalFromMap}";
        }
    }
}
