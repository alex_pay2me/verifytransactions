﻿using System;
using System.Collections.Generic;

namespace VerifyTransactions.GUI
{
    internal class TransactionDifference
    {
        /// <summary>
        /// Дата операции
        /// </summary>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        /// Номер заказа мерчанта
        /// </summary>
        public string MerchantOrderID { get; set; }

        /// <summary>
        /// Ref number (RRN)
        /// </summary>
        public string RefNumber { get; set; }

        /// <summary>
        /// Сумма транзакции
        /// </summary>
        public double Amount { get; set; }

        /// <summary>
        /// Маска карты
        /// </summary>
        public string PANMask { get; set; }

        /// <summary>
        /// Расхождение
        /// </summary>
        public List<Differences> Defferences { get; set; }
    }
}
