﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Connector.Map.Api.Models;
using Connector.Map.Api.Services;
using OfficeOpenXml;
using VerifyTransactions.DataReaders;
using VerifyTransactions.DataReaders.Models;
using VerifyTransactions.GUI.Controls;

namespace VerifyTransactions.GUI.Forms
{
    public partial class MainForm : Form
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ITheMapApiClient _mapApiClient;
        private readonly AuthForm _authForm;
        private readonly DataReadersFactory _readersFactory;

        /// <summary>
        /// Key: ProcessingId
        /// </summary>
        private readonly Dictionary<long, List<Merchant>> _processingMerchantsMap = new();
        /// <summary>
        /// Key: MerchantId
        /// </summary>
        private readonly Dictionary<long, List<Terminal>> _merchantTerminalsMap = new();

        public MainForm(IServiceProvider serviceProvider
            , AuthForm authForm
            , ITheMapApiClient mapApiClient
            , DataReadersFactory readersFactory)
        {
            InitializeComponent();

            _authForm = authForm;
            _mapApiClient = mapApiClient;
            _readersFactory = readersFactory;
            _serviceProvider = serviceProvider;

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        private async void MainFormNew_Load(object sender, EventArgs e)
        {
            if (_authForm.ShowDialog() != DialogResult.OK)
            {
                Close();
                return;
            }

            try
            {
                //btnVerify.Enabled = false;

                List<Processing> processings = await _mapApiClient.GetProcessingsAsync(new GetProcessingsRequest());
                processings.RemoveAll(p => p.Id != 14 && p.Id != 39);

                if (processings.Count == 0)
                    throw new Exception("У авторизованного пользователя не задано ни одного процессинга!");

                List<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones().ToList();
                List<Merchant> merchants = await _mapApiClient.GetMerchantsAsync(new GetMerchantsRequest());
                List<Terminal> terminals = await _mapApiClient.GetTerminalsAsync(new GetTerminalsRequest());

                InitMaps(merchants, terminals);

                actualizeDataControl1.Initialize(_mapApiClient, processings, _processingMerchantsMap, _merchantTerminalsMap);

                DateTime yesterday = DateTime.Now.Date.AddDays(-1);

                Processing jysan = processings.FirstOrDefault(p => p.Name == "JysanBank");
                DisableControl(jysan == null, btnVerifyJysan, verifyOptionsControlJysan);
                verifyOptionsControlJysan.Initialize(jysan, timeZones, _processingMerchantsMap.ContainsKey(jysan?.Id ?? 0) ? _processingMerchantsMap[jysan.Id] : new List<Merchant>(), 
                    _merchantTerminalsMap, startDate: yesterday.AddHours(-1).AddMilliseconds(-1));

                Processing alfa = processings.FirstOrDefault(p => p.Name == "AlfabankKZ");
                DisableControl(alfa == null, btnVerifyAlfa, verifyOptionsControlAlfa);
                verifyOptionsControlAlfa.Initialize(alfa, timeZones, _processingMerchantsMap.ContainsKey(alfa?.Id ?? 0) ? _processingMerchantsMap[alfa.Id] : new List<Merchant>(), 
                    _merchantTerminalsMap, startDate: yesterday);

            }
            catch (Exception ex)
            {
                MessageBox.Show(this, $"Ошибка инициализации приложения: {ex.Message}");
                Close();
            }
        }

        private static void DisableControl(bool disable, Button disableButton, VerifyOptionsControl optionsControl)
        {
            if (!disable)
                return;

            disableButton.Enabled = false;
            optionsControl.Visible = false;
        }

        private void InitMaps(List<Merchant> merchants, List<Terminal> terminals)
        {
            Dictionary<long, Merchant> merchantsMap = merchants.ToDictionary(x => x.Id, y => y);

            foreach (var processingTerminals in terminals.GroupBy(t => t.ProcessingId))
            {
                long processingId = processingTerminals.Key;
                if (!_processingMerchantsMap.ContainsKey(processingId))
                    _processingMerchantsMap[processingId] = new List<Merchant>();

                foreach (var merchantTerminals in processingTerminals.GroupBy(t => t.MerchantId))
                {
                    long merchantId = merchantTerminals.Key;
                    if (!_merchantTerminalsMap.ContainsKey(merchantId))
                        _merchantTerminalsMap[merchantId] = new List<Terminal>();

                    if (!merchantsMap.TryGetValue(merchantId, out var merchant))
                        continue;

                    _processingMerchantsMap[processingId].Add(merchant);

                    foreach (var terminal in merchantTerminals)
                    {
                        _merchantTerminalsMap[merchantId].Add(terminal);
                    }
                }
            }
        }

        private void OnStartVerificationJysan_Click(object sender, EventArgs e)
        {
            var options = verifyOptionsControlJysan.GetSelectedOptions();

            StartVerification(options);
        }

        private void OnStartVerificationAlfa_Click(object sender, EventArgs e)
        {
            var options = verifyOptionsControlAlfa.GetSelectedOptions();

            StartVerification(options);
        }

        private async void StartVerification(VerifyOptions options)
        {
            try
            {
                if ((options.DateEnd - options.DateStart).TotalDays > 4D)
                    throw new Exception("Выбранный интервал не может быть больше 4 суток");

                if (options.ExcelFiles?.Length is null or 0)
                    throw new Exception("Не выбран файл с суточным реестром");

                if (options.Merchant == null)
                    throw new Exception("Не выбрано предприятие");

                if (options.Terminal == null)
                    throw new Exception("Не выбран терминал");

                if (options.TimeZone == null)
                    throw new Exception("Не выбран часовой пояс");

                tabControl1.Enabled = false;
                SetLoadingVisibility(true);

                IDataReader reader = _readersFactory.CreateReader(options.Processing.Name, DataFormat.XLSX);

                ReadOptions readOptions = new()
                {
                    TimeZone = options.TimeZone,
                    FilePath = options.ExcelFiles[0]
                };

                Dictionary<string, TransactionItem> transactionsFromMerchant = await reader.ReadTransactionsAsync(readOptions);
                List<TransactionDifference> differences = new();

                DifferenceCounter counter = new()
                {
                    TotalFromBank = transactionsFromMerchant.Count
                };

                // Максимальное количество транзакций, которое можно за раз запросить от TheMAP
                const int limit = 50;

                int[] statuses = options.Processing.Name == "JysanBank"
                    ? new int[] { (int)Status.Debited, (int)Status.Refunded }
                    : new int[] { (int)Status.Paid, (int)Status.Rejected };

                TransactionsRequest request = new()
                {
                    Merchants = new int[] { (int)options.Merchant.Id },
                    Terminals = new int[] { (int)options.Terminal.Id },
                    States = statuses,
                    Limit = limit,
                    DateRange = GetDateRange(GetDateUtcFromTimeZone(options.DateStart, options.TimeZone), GetDateUtcFromTimeZone(options.DateEnd, options.TimeZone))
                };

                Progress progress = new(transactionsFromMerchant.Count);

                List<Transaction> lastTransactions = null;
                do
                {
                    lastTransactions = await _mapApiClient.GetTransactionsAsync(request);

                    if (lastTransactions?.Count > 0)
                    {
                        request.OrderIdDown = lastTransactions.Last().Id;
                        counter.TotalFromMap += lastTransactions.Count;
                    }

                    FindDifferences(transactionsFromMerchant, lastTransactions, differences, counter);

                    progress.CountDone += lastTransactions?.Count ?? 0;

                    lbPercent.Text = $"{progress.Percentage:N2}%...";
                    lbTime.Text = progress.RemainingTime;

                } while (lastTransactions?.Count == limit);

                counter.OutOfDateRangeInMAP = transactionsFromMerchant.Count;

                if (transactionsFromMerchant.Count < 100)
                {
                    await SearchInMap(transactionsFromMerchant, (int)options.Merchant.Id, (int)options.Terminal.Id, counter, differences);
                }

                foreach (var merchantTransaction in transactionsFromMerchant.Values)
                {
                    AddDifference(differences, merchantTransaction, new List<Differences> { Differences.NotFoundInMAP });
                    counter.Differences++;
                }

                if (differences.Count > 0)
                {
                    string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Reports", $"Результат сверки {options.Terminal.Name} {DateTime.Now:dd-MM-yy HH-mm-ss}.xlsx");

                    SaveToExcelFile(differences, path);

                    if (counter.OutOfDateRangeInMAP > 100)
                    {
                        MessageBox.Show(this, $"Вероятно ошибка с периодом, {counter.OutOfDateRangeInMAP} транзакций не найдено в МАР в данном периоде.");
                    }

                    using Process fileopener = new();
                    fileopener.StartInfo.FileName = "explorer";
                    fileopener.StartInfo.Arguments = $"\"{path}\"";
                    fileopener.Start();
                }

                new ResultForm(counter.ToString())
                    .ShowDialog(this);
            }
            catch (Exception ex)
            {
                SetLoadingVisibility(false);
                MessageBox.Show(this, $"Возникла ошибка: {ex.Message}");
            }
            finally
            {
                tabControl1.Enabled = true;
                SetLoadingVisibility(false);
            }
        }

        /// <summary>
        /// Пробуем найти транзакции по RRN
        /// </summary>
        /// <param name="transactionsFromMerchant"></param>
        /// <param name="request"></param>
        /// <param name="counter"></param>
        /// <param name="differences"></param>
        /// <returns></returns>
        private async Task SearchInMap(Dictionary<string, TransactionItem> transactionsFromMerchant, int merchantId, int terminalId, DifferenceCounter counter, List<TransactionDifference> differences)
        {
            TransactionsRequest request = new()
            {
                Merchants = new int[] { merchantId },
                Terminals = new int[] { terminalId },
                RRNs = new string[] { "" },
                Limit = 10
            };

            Progress progress = new(transactionsFromMerchant.Count);

            foreach (var merchantTransaction in transactionsFromMerchant.Values.ToList())
            {
                request.RRNs[0] = merchantTransaction.RefNumber;

                List<Transaction> result = await _mapApiClient.GetTransactionsAsync(request);

                progress.CountDone++;

                lbPercent.Text = $"{progress.Percentage:N2}%...";
                lbTime.Text = progress.RemainingTime;

                if (result?.Count is null or 0)
                    continue;

                Transaction mapTransaction = result[0];

                List<Differences> difference = GetDifferences(mapTransaction, merchantTransaction, Differences.NotInRange);

                if (difference.Count > 0 && difference[0] != Differences.NotInRange)
                    counter.Differences++;

                AddDifference(differences, mapTransaction, difference);

                transactionsFromMerchant.Remove(merchantTransaction.RefNumber);
            }
        }

        /// <summary>
        /// Получить список расхождений между транзакциями
        /// </summary>
        /// <param name="mapTransaction">Транзакция из TheMAP</param>
        /// <param name="merchantTransaction">Транзакция из выписки банка</param>
        /// <param name="defaultValue">Расхождение по умолчанию, если других расхождение не найдено</param>
        /// <returns></returns>
        private static List<Differences> GetDifferences(Transaction mapTransaction, TransactionItem merchantTransaction, Differences? defaultValue = null)
        {
            List<Differences> result = new();

            if (mapTransaction.Amount != merchantTransaction.Amount)
                result.Add(Differences.DifferentAmount);

            if (mapTransaction.StateID != (int)merchantTransaction.Status)
                result.Add(Differences.DifferentState);

            if (result.Count == 0 && defaultValue.HasValue)
                result.Add(defaultValue.Value);

            return result;
        }

        /// <summary>
        /// Установить видимость панели прогресса
        /// </summary>
        /// <param name="visible">Видимость панели</param>
        void SetLoadingVisibility(bool visible)
        {
            if (InvokeRequired)
                BeginInvoke(new Action(() => { SetLoadingVisibility(visible); }));
            else
            {
                Point new_location = new(Width / 2 - _reloadingPanel.Width / 2, Height / 2 - _reloadingPanel.Height / 2);
                _reloadingPanel.Location = new_location;
                _reloadingPanel.Visible = visible;
            }
        }

        private static void SaveToExcelFile(List<TransactionDifference> differences, string path)
        {
            string reportsPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Reports");
            if (!Directory.Exists(reportsPath))
                Directory.CreateDirectory(reportsPath);

            using ExcelPackage document = new(new FileInfo(path));
            using ExcelWorksheet sheet = document.Workbook.Worksheets.Add("Report");

            sheet.Cells[1, 1].Value = "Дата операции";
            sheet.Cells[1, 2].Value = "Номер карты";
            sheet.Cells[1, 3].Value = "Номер заказа мерчанта";
            sheet.Cells[1, 4].Value = "Ref number (RRN)";
            sheet.Cells[1, 5].Value = "Сумма транзакции";
            sheet.Cells[1, 6].Value = "Различие";

            for (int i = 0; i < differences.Count; i++)
            {
                sheet.Cells[i + 2, 1].Style.Numberformat.Format = "dd.MM.yyyy HH:mm:ss";
                sheet.Cells[i + 2, 1].Value = differences[i].TimeStamp.ToOADate();


                sheet.Cells[i + 2, 2].Value = differences[i].PANMask;
                sheet.Cells[i + 2, 3].Value = differences[i].MerchantOrderID;
                sheet.Cells[i + 2, 4].Value = differences[i].RefNumber;
                sheet.Cells[i + 2, 5].Value = differences[i].Amount;
                sheet.Cells[i + 2, 6].Value = string.Join(", ", differences[i].Defferences.Select(d => GetStringView(d)));
            }

            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();

            document.Save();
        }

        static string GetStringView(Differences defferences)
        {
            return defferences switch
            {
                Differences.DifferentAmount => "Расхождение по сумме",
                Differences.DifferentState => "Расхождение по статусу",
                Differences.NotFoundInBankStatement => "Нет в выписке",
                Differences.NotFoundInMAP => "Нет в МАР",
                Differences.NotInRange => "Не попал в интервал",
                _ => throw new NotImplementedException()
            };
        }

        private static void FindDifferences(Dictionary<string, TransactionItem> merchantTransactions,
            List<Transaction> mapTransactions, List<TransactionDifference> differences, DifferenceCounter counter)
        {
            if (mapTransactions?.Count is null or 0)
                return;

            for (int i = 0; i < mapTransactions.Count; i++)
            {
                Transaction mapTransaction = mapTransactions[i];

                if (merchantTransactions.TryGetValue(mapTransaction.RRN, out var merchantTransaction))
                {
                    List<Differences> difference = GetDifferences(mapTransaction, merchantTransaction);

                    if (difference.Count > 0)
                    {
                        AddDifference(differences, mapTransaction, difference);
                        counter.Differences++;
                    }

                    merchantTransactions.Remove(mapTransaction.RRN);
                    continue;
                }

                AddDifference(differences, mapTransaction, new List<Differences> { Differences.NotFoundInBankStatement });
            }
        }

        private static void AddDifference(List<TransactionDifference> differences, Transaction fromMap, List<Differences> difference)
        {
            TransactionDifference dif = new()
            {
                Amount = fromMap.Amount / 100,
                RefNumber = fromMap.RRN,
                TimeStamp = fromMap.DateCreated.ToLocalTime(),
                MerchantOrderID = fromMap?.MerchantOrderID,
                Defferences = difference,
                PANMask = fromMap.PANMask
            };

            differences.Add(dif);
        }

        private static void AddDifference(List<TransactionDifference> differences, TransactionItem fromMerchant, List<Differences> difference)
        {
            TransactionDifference dif = new()
            {
                Amount = fromMerchant.Amount / 100,
                RefNumber = fromMerchant.RefNumber,
                TimeStamp = fromMerchant.TimeStamp.ToLocalTime(),
                MerchantOrderID = null,
                Defferences = difference,
                PANMask = fromMerchant.PANMask
            };

            differences.Add(dif);
        }

        private DateTime GetDateUtcFromTimeZone(DateTime date, TimeZoneInfo sourceZone)
        {
            if (date.Kind == DateTimeKind.Local && sourceZone.Equals(TimeZoneInfo.Local))
                return date.ToUniversalTime();

            return TimeZoneInfo.ConvertTimeToUtc(date, sourceZone);
        }

        private static string[] GetDateRange(DateTime dateFrom, DateTime dateTo)
        {
            return new string[] { dateFrom.ToString("O"), dateTo.ToString("O") };
        }

    }
}
