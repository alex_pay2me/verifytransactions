﻿
namespace VerifyTransactions.GUI.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tbActualize = new System.Windows.Forms.TabPage();
            this.actualizeDataControl1 = new VerifyTransactions.GUI.Controls.ActualizeDataControl();
            this.tabJysan = new System.Windows.Forms.TabPage();
            this.verifyOptionsControlJysan = new VerifyTransactions.GUI.Controls.VerifyOptionsControl();
            this.btnVerifyJysan = new System.Windows.Forms.Button();
            this.tabAlfa = new System.Windows.Forms.TabPage();
            this.btnVerifyAlfa = new System.Windows.Forms.Button();
            this.verifyOptionsControlAlfa = new VerifyTransactions.GUI.Controls.VerifyOptionsControl();
            this._reloadingPanel = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lbTime = new System.Windows.Forms.Label();
            this.lbPercent = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tbActualize.SuspendLayout();
            this.tabJysan.SuspendLayout();
            this.tabAlfa.SuspendLayout();
            this._reloadingPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tbActualize);
            this.tabControl1.Controls.Add(this.tabJysan);
            this.tabControl1.Controls.Add(this.tabAlfa);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(6, 7);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(762, 486);
            this.tabControl1.TabIndex = 0;
            // 
            // tbActualize
            // 
            this.tbActualize.Controls.Add(this.actualizeDataControl1);
            this.tbActualize.Location = new System.Drawing.Point(4, 29);
            this.tbActualize.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbActualize.Name = "tbActualize";
            this.tbActualize.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tbActualize.Size = new System.Drawing.Size(754, 453);
            this.tbActualize.TabIndex = 0;
            this.tbActualize.Text = "  Актуализация   ";
            this.tbActualize.UseVisualStyleBackColor = true;
            // 
            // actualizeDataControl1
            // 
            this.actualizeDataControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.actualizeDataControl1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.actualizeDataControl1.Location = new System.Drawing.Point(3, 4);
            this.actualizeDataControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.actualizeDataControl1.Name = "actualizeDataControl1";
            this.actualizeDataControl1.Padding = new System.Windows.Forms.Padding(5);
            this.actualizeDataControl1.Size = new System.Drawing.Size(748, 445);
            this.actualizeDataControl1.TabIndex = 0;
            // 
            // tabJysan
            // 
            this.tabJysan.Controls.Add(this.verifyOptionsControlJysan);
            this.tabJysan.Controls.Add(this.btnVerifyJysan);
            this.tabJysan.Location = new System.Drawing.Point(4, 29);
            this.tabJysan.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabJysan.Name = "tabJysan";
            this.tabJysan.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabJysan.Size = new System.Drawing.Size(754, 453);
            this.tabJysan.TabIndex = 1;
            this.tabJysan.Text = "  Jysan  ";
            this.tabJysan.UseVisualStyleBackColor = true;
            // 
            // verifyOptionsControlJysan
            // 
            this.verifyOptionsControlJysan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.verifyOptionsControlJysan.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.verifyOptionsControlJysan.Location = new System.Drawing.Point(1, 1);
            this.verifyOptionsControlJysan.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.verifyOptionsControlJysan.MinimumSize = new System.Drawing.Size(525, 290);
            this.verifyOptionsControlJysan.Name = "verifyOptionsControlJysan";
            this.verifyOptionsControlJysan.Padding = new System.Windows.Forms.Padding(5);
            this.verifyOptionsControlJysan.Size = new System.Drawing.Size(747, 290);
            this.verifyOptionsControlJysan.TabIndex = 19;
            // 
            // btnVerifyJysan
            // 
            this.btnVerifyJysan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVerifyJysan.Location = new System.Drawing.Point(580, 417);
            this.btnVerifyJysan.Name = "btnVerifyJysan";
            this.btnVerifyJysan.Size = new System.Drawing.Size(168, 29);
            this.btnVerifyJysan.TabIndex = 18;
            this.btnVerifyJysan.Text = "Запустить сверку";
            this.btnVerifyJysan.UseVisualStyleBackColor = true;
            this.btnVerifyJysan.Click += new System.EventHandler(this.OnStartVerificationJysan_Click);
            // 
            // tabAlfa
            // 
            this.tabAlfa.Controls.Add(this.btnVerifyAlfa);
            this.tabAlfa.Controls.Add(this.verifyOptionsControlAlfa);
            this.tabAlfa.Location = new System.Drawing.Point(4, 29);
            this.tabAlfa.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabAlfa.Name = "tabAlfa";
            this.tabAlfa.Size = new System.Drawing.Size(754, 453);
            this.tabAlfa.TabIndex = 2;
            this.tabAlfa.Text = "  Альфа-Банк  ";
            this.tabAlfa.UseVisualStyleBackColor = true;
            // 
            // btnVerifyAlfa
            // 
            this.btnVerifyAlfa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVerifyAlfa.Location = new System.Drawing.Point(580, 417);
            this.btnVerifyAlfa.Name = "btnVerifyAlfa";
            this.btnVerifyAlfa.Size = new System.Drawing.Size(168, 29);
            this.btnVerifyAlfa.TabIndex = 19;
            this.btnVerifyAlfa.Text = "Запустить сверку";
            this.btnVerifyAlfa.UseVisualStyleBackColor = true;
            this.btnVerifyAlfa.Click += new System.EventHandler(this.OnStartVerificationAlfa_Click);
            // 
            // verifyOptionsControlAlfa
            // 
            this.verifyOptionsControlAlfa.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.verifyOptionsControlAlfa.Location = new System.Drawing.Point(1, 1);
            this.verifyOptionsControlAlfa.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.verifyOptionsControlAlfa.MinimumSize = new System.Drawing.Size(525, 290);
            this.verifyOptionsControlAlfa.Name = "verifyOptionsControlAlfa";
            this.verifyOptionsControlAlfa.Padding = new System.Windows.Forms.Padding(5);
            this.verifyOptionsControlAlfa.Size = new System.Drawing.Size(747, 290);
            this.verifyOptionsControlAlfa.TabIndex = 0;
            // 
            // _reloadingPanel
            // 
            this._reloadingPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._reloadingPanel.Controls.Add(this.lbPercent);
            this._reloadingPanel.Controls.Add(this.lbTime);
            this._reloadingPanel.Controls.Add(this.label1);
            this._reloadingPanel.Controls.Add(this.label8);
            this._reloadingPanel.Controls.Add(this.pictureBox1);
            this._reloadingPanel.Location = new System.Drawing.Point(300, 214);
            this._reloadingPanel.Name = "_reloadingPanel";
            this._reloadingPanel.Size = new System.Drawing.Size(240, 72);
            this._reloadingPanel.TabIndex = 16;
            this._reloadingPanel.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(73, 11);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 25);
            this.label8.TabIndex = 1;
            this.label8.Text = "Сверка";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::VerifyTransactions.GUI.Properties.Resources.preloader;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(64, 64);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(73, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Осталось - ";
            // 
            // lbTime
            // 
            this.lbTime.AutoSize = true;
            this.lbTime.Location = new System.Drawing.Point(153, 41);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(63, 20);
            this.lbTime.TabIndex = 3;
            this.lbTime.Text = "00:00:00";
            // 
            // lbPercent
            // 
            this.lbPercent.AutoSize = true;
            this.lbPercent.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbPercent.Location = new System.Drawing.Point(153, 11);
            this.lbPercent.Name = "lbPercent";
            this.lbPercent.Size = new System.Drawing.Size(50, 25);
            this.lbPercent.TabIndex = 4;
            this.lbPercent.Text = "0%...";
            // 
            // MainFormNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 500);
            this.Controls.Add(this._reloadingPanel);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimumSize = new System.Drawing.Size(790, 450);
            this.Name = "MainFormNew";
            this.Padding = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Сверка транзакций";
            this.Load += new System.EventHandler(this.MainFormNew_Load);
            this.tabControl1.ResumeLayout(false);
            this.tbActualize.ResumeLayout(false);
            this.tabJysan.ResumeLayout(false);
            this.tabAlfa.ResumeLayout(false);
            this._reloadingPanel.ResumeLayout(false);
            this._reloadingPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tbActualize;
        private System.Windows.Forms.TabPage tabJysan;
        private System.Windows.Forms.TabPage tabAlfa;
        private System.Windows.Forms.Button btnVerifyJysan;
        private Controls.ActualizeDataControl actualizeDataControl1;
        private Controls.VerifyOptionsControl verifyOptionsControlJysan;
        private System.Windows.Forms.Button btnVerifyAlfa;
        private Controls.VerifyOptionsControl verifyOptionsControlAlfa;
        private System.Windows.Forms.Panel _reloadingPanel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbPercent;
        private System.Windows.Forms.Label lbTime;
    }
}