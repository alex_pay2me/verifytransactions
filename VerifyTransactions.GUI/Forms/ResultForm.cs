﻿using System.Windows.Forms;

namespace VerifyTransactions.GUI.Forms
{
    public partial class ResultForm : Form
    {
        public ResultForm(string resultText)
        {
            InitializeComponent();

            richTextBox1.Text = resultText;
        }
    }
}
