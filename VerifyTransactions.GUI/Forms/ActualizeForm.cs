﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Connector.Map.Api.Models;
using Connector.Map.Api.Services;
using VerifyTransactions.GUI.ViewModels;

namespace VerifyTransactions.GUI.Forms
{
    public partial class ActualizeForm : Form
    {
        private readonly ITheMapApiClient _mapApiClient;
        private Terminal _terminal;

        readonly private List<MerchantOrderStateView> _ordersStateDataSource = new();

        public ActualizeForm(ITheMapApiClient mapApiClient)
        {
            InitializeComponent();

            _mapApiClient = mapApiClient;
            dataGridView1.AutoGenerateColumns = false;
        }

        public void SetTerminal(Terminal terminal)
        {
            _terminal = terminal;

            Text = $"Актуализация заказов терминала {terminal.Name}";
        }

        private async void OnActualizeButton_Click(object sender, EventArgs e)
        {
            try
            {
                button2.Enabled = false;

                List<string> merchantOrders = GerMerchantOrderIds();
                if (merchantOrders.Count == 0)
                {
                    throw new Exception("Введите номера заказов мерчанта. Каждый номер с новой строки!");
                }

                SetLoadingVisibility(true);

                List<ActualizeOrderResponce> merchantOrdersStates = await GetOrderStates(merchantOrders);

                _ordersStateDataSource.Clear();
                _ordersStateDataSource.AddRange(merchantOrdersStates.Select(x => new MerchantOrderStateView(x)));

                RefreshDataSource();
            }
            catch (Exception ex)
            {
                SetLoadingVisibility(false);
                MessageBox.Show(this, ex.Message);
            }
            finally
            {
                button2.Enabled = true;
                SetLoadingVisibility(false);
            }
        }

        private async Task<List<ActualizeOrderResponce>> GetOrderStates(List<string> merchantOrders)
        {
            List<ActualizeOrderResponce> result = new(merchantOrders.Count);

            ActualizeOrderRequest request = new()
            {
                Key = _terminal.Name,
                IsOverload = true
            };

            foreach (var orderId in merchantOrders)
            {
                request.MerchantOrderId = orderId;

                ActualizeOrderResponce responce = await _mapApiClient.GetMerchantOrderStateAsync(request);

                responce.MerchantOrderId = orderId;

                result.Add(responce);
            }

            return result;
        }

        private void RefreshDataSource()
        {
            if (InvokeRequired)
                Invoke(new Action(RefreshDataSource));
            else
            {
                dataGridView1.DataSource = null;
                dataGridView1.DataSource = _ordersStateDataSource;
            }
        }

        /// <summary>
        /// Установить видимость панели прогресса
        /// </summary>
        /// <param name="visible">Видимость панели</param>
        void SetLoadingVisibility(bool visible)
        {
            if (InvokeRequired)
                BeginInvoke(new Action(() => { SetLoadingVisibility(visible); }));
            else
            {
                Point new_location = new(Width / 2 - _reloadingPanel.Width / 2, Height / 2 - _reloadingPanel.Height / 2);
                _reloadingPanel.Location = new_location;
                _reloadingPanel.Visible = visible;
            }
        }

        private List<string> GerMerchantOrderIds()
        {
            return richTextBox1.Lines
                .Select(line => line.Trim())
                .ToList();
        }

        private void ActualizeForm_SizeChanged(object sender, EventArgs e)
        {
            if (_reloadingPanel.Visible)
            {
                Point new_location = new(Width / 2 - _reloadingPanel.Width / 2, Height / 2 - _reloadingPanel.Height / 2);
                _reloadingPanel.Location = new_location;
            }
        }
    }
}
