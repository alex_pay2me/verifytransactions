﻿using System;
using System.IO;
using System.Windows.Forms;
using Connector.Map.Api.Services;
using VerifyTransactions.GUI.Helpers;


namespace VerifyTransactions.GUI.Forms
{
    public partial class AuthForm : Form
    {
        private static readonly string SavedAccountPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "autocomplete");

        private readonly IAuthService _authService;

        public AuthForm(IAuthService authService)
        {
            _authService = authService;

            InitializeComponent();
        }

        private async void OnAuthButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(tbLogin.Text))
                    throw new Exception("Не задан логин");
                if (string.IsNullOrEmpty(tbPassword.Text))
                    throw new Exception("Не задан пароль");

                btnAuth.Enabled = false;

                await _authService.Authentificate(tbLogin.Text, tbPassword.Text);

                if (chkRemember.Checked)
                {
                    CredentialUtil.SetCredentials("VerifyTransactions.GUI", tbLogin.Text.Trim(), tbPassword.Text, CredentialManagement.PersistanceType.LocalComputer);
                }
                else
                {
                    CredentialUtil.RemoveCredentials("VerifyTransactions.GUI");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, $"Ошибка авторизации: {ex.Message}");
                return;
            }
            finally
            {
                btnAuth.Enabled = true;
            }

            DialogResult = DialogResult.OK;
        }

        private void OnAuthForm_Load(object sender, EventArgs e)
        {
            try
            {
                var _authAccount = CredentialUtil.GetCredential("VerifyTransactions.GUI");

                tbLogin.Text = _authAccount?.Login ?? "";
                tbPassword.Text = _authAccount?.Password ?? "";
            }
            catch (Exception)
            { }
        }

        internal void SaveAuthAccount(string login, string password)
        {
            try
            {
                File.WriteAllText(SavedAccountPath, JsonSerializer.WriteFromObject(new SavedAccount(login, null)));

                CredentialUtil.SetCredentials("VerifyTransactions.GUI", login, password, CredentialManagement.PersistanceType.LocalComputer);
            }
            catch (Exception)
            { }
        }
    }
}
