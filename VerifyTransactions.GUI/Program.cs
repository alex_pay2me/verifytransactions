using System;
using System.Windows.Forms;
using Connector.Map.Api.IoC;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VerifyTransactions.DataReaders.IoC;
using VerifyTransactions.GUI.Forms;

namespace VerifyTransactions.GUI
{
    static class Program
    {        
        public static IServiceProvider ServiceProvider { get; set; }
        public static IConfiguration Configuration { get; set; }

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            var services = new ServiceCollection();

            ConfigureServices(services);

            ServiceProvider = services.BuildServiceProvider();

            var mainForm = ServiceProvider.GetRequiredService<MainForm>();
            
            Application.Run(mainForm);
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging();
            services.AddTheMapConnector(Configuration);
            services.AddDataReaders(Configuration);

            services.AddScoped<MainForm>();
            services.AddScoped<AuthForm>();
            services.AddScoped<ActualizeForm>();
        }
    }
}
