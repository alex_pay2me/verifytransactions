﻿using System;
using System.ComponentModel;

namespace VerifyTransactions.GUI
{
    /// <summary>
    /// Прогресс выполнения задачи
    /// </summary>
    public class Progress : INotifyPropertyChanged
    {
        public Progress()
        { }

        public Progress(long _TotalCount)
        {
            TotalCount = _TotalCount;
        }

        public void OnPropertyChanged(PropertyChangedEventArgs e)
            => PropertyChanged?.Invoke(this, e);

        public event PropertyChangedEventHandler PropertyChanged;

        private DateTime startTime = DateTime.Now;

        public bool ShowPercenage { get; set; } = true;

        public string ProgressInfo
        {
            get
            {
                string info = ActionName;
                if (ShowPercenage && Percentage > 0D)
                    info += $" {Percentage:N2}%";
                else if(CountDone > 0)
                    info += $" {CountDone} из {TotalCount}";

                return info;
            }
        }

        public bool InProgrees
        {
            get { return CountDone > 0; }
        }

        public string RemainingTime
        {
            get
            {
                double seconds_passed = (DateTime.Now - startTime).TotalSeconds;
                if (CountDone > 0 && seconds_passed > 2)
                {
                    double total_seconds = TotalCount * (seconds_passed / CountDone);

                    return TimeSpan.FromSeconds(total_seconds - seconds_passed).ToString(@"hh\:mm\:ss");
                }

                return "00:00:00";
            }
        }

        public double Percentage => (TotalCount > 0) ? CountDone / (double)TotalCount * 100D : 0D;

        long _TotalCount = 0;
        public long TotalCount
        {
            get { return _TotalCount; }
            set
            {
                _TotalCount = value;
                startTime = DateTime.Now;
                OnPropertyChanged(new PropertyChangedEventArgs(nameof(ProgressInfo)));
            }
        }

        public string _ActionName = "Готов к работе!";
        public string ActionName
        {
            get { return _ActionName; }
            set
            {
                if (_ActionName == value)
                    return;
                _ActionName = value;
                startTime = DateTime.Now;
                OnPropertyChanged(new PropertyChangedEventArgs(nameof(ActionName)));
                OnPropertyChanged(new PropertyChangedEventArgs(nameof(ProgressInfo)));
            }
        }       

        long _CountDone = 0;
        public long CountDone
        {
            get { return _CountDone; }
            set
            {
                _CountDone = value;
                OnPropertyChanged(new PropertyChangedEventArgs(nameof(ProgressInfo)));
                OnPropertyChanged(new PropertyChangedEventArgs(nameof(RemainingTime)));
                OnPropertyChanged(new PropertyChangedEventArgs(nameof(InProgrees)));
            }
        }

        public void Increase() => CountDone++;

        public void Reset()
        {
            ActionName = "Готов к работе!";
            CountDone = 0;
            TotalCount = 0;
        }        
    }
}
