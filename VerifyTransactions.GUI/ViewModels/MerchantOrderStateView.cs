﻿using Connector.Map.Api.Models;

namespace VerifyTransactions.GUI.ViewModels
{
    /// <summary>
    /// Представление статуса заказа Продавца (Мерчанта)
    /// </summary>
    internal class MerchantOrderStateView
    {
        readonly ActualizeOrderResponce _orderState;

        public MerchantOrderStateView(ActualizeOrderResponce orderState)
        {
            _orderState = orderState;
        }

        /// <summary>
        /// Идентификатор заказа у мерчанта
        /// </summary>
        public string MerchantOrderId => _orderState.MerchantOrderId;

        /// <summary>
        /// Статус выполнения операции
        /// </summary>
        public string Success => _orderState.Success ? "Да" : "Нет";

        /// <summary>
        /// Статус 
        /// </summary>
        public string State => _orderState.State;

        /// <summary>
        /// Reference Retrieval Number
        /// </summary>
        public string RRN => _orderState.RRN;
    }
}
