﻿using System;

namespace VerifyTransactions.GUI
{
    internal class SavedAccount
    {
        public SavedAccount(string login, string password)
        {
            Login = login ?? throw new ArgumentNullException(nameof(login));
            Password = password ?? throw new ArgumentNullException(nameof(password));
        }

        public string Login { get; set; }
        
        public string Password { get; set; }
    }
}
