﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Connector.Map.Api.Models;

namespace Connector.Map.Api.Services
{
    /// <summary>
    /// Клиент для работы с API TheMAP
    /// </summary>
    public interface ITheMapApiClient
    {
        /// <summary>
        /// Запросить список транзакций
        /// </summary>
        /// <param name="requestParams">Параметры запроса</param>
        /// <param name="cancellationToken">Токен отмены операции</param>
        /// <returns></returns>
        Task<List<Transaction>> GetTransactionsAsync(TransactionsRequest requestParams, CancellationToken cancellationToken = default);

        /// <summary>
        /// Запрос истории состояний транзакции
        /// </summary>
        /// <param name="requestParams">Параметры запроса</param>
        /// <param name="cancellationToken">Токен отмены операции</param>
        /// <returns></returns>
        Task<List<TransactionTrace>> GetTransactionTraceAsync(TransactionTraceRequest requestParams, CancellationToken cancellationToken = default);

        /// <summary>
        /// Запросить список предприятий
        /// </summary>
        /// <param name="requestParams">Параметры запроса</param>
        /// <param name="cancellationToken">Токен отмены операции</param>
        /// <returns></returns>
        Task<List<Merchant>> GetMerchantsAsync(GetMerchantsRequest requestParams, CancellationToken cancellationToken = default);

        /// <summary>
        /// Запросить список процессингов
        /// </summary>
        /// <param name="requestParams">Параметры запроса</param>
        /// <param name="cancellationToken">Токен отмены операции</param>
        /// <returns></returns>
        Task<List<Processing>> GetProcessingsAsync(GetProcessingsRequest requestParams, CancellationToken cancellationToken = default);

        /// <summary>
        /// Запросить список терминалов
        /// </summary>
        /// <param name="requestParams">Параметры запроса</param>
        /// <param name="cancellationToken">Токен отмены операции</param>
        /// <returns></returns>
        Task<List<Terminal>> GetTerminalsAsync(GetTerminalsRequest requestParams, CancellationToken cancellationToken = default);

        /// <summary>
        /// Запросить статус заказа у мерчанта
        /// </summary>
        /// <param name="requestParams">Параметры запроса</param>
        /// <param name="cancellationToken">Токен отмены операции</param>
        /// <returns></returns>
        Task<ActualizeOrderResponce> GetMerchantOrderStateAsync(ActualizeOrderRequest requestParams, CancellationToken cancellationToken = default);
    }
}
