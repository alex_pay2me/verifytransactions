﻿using System.Threading;
using System.Threading.Tasks;

namespace Connector.Map.Api.Services
{
    public interface IAuthService
    {
        /// <summary>
        /// Выполнена успешная аутентификация
        /// </summary>
        public bool Authentificated { get; }

        /// <summary>
        /// AccessToken, полученный при аутентификации
        /// </summary>
        public string AccessToken { get; }

        /// <summary>
        /// Выполнить аутентфикацию
        /// </summary>
        /// <param name="login">Логин</param>
        /// <param name="password">Пароль</param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task Authentificate(string login, string password, CancellationToken cancellationToken = default);

        /// <summary>
        /// Обновить AccessToken с использованием RefreshToken
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task RefreshIdTokenAsync(CancellationToken cancellationToken = default);        
    }
}
