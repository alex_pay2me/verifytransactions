﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Connector.Map.Api.Configurations.AppSettings;
using Connector.Map.Api.Models;
using Flurl.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;

namespace Connector.Map.Api.Services.Impl
{
    internal class AuthService : IAuthService
    {
        /// <summary>
        /// Ошибка при отправке просроченного рефреш токена
        /// </summary>
        const string RefreshExpiredError = "invalid_grant";

        readonly TheMapAuthOptions _authOptions;
        readonly ILogger<AuthService> _logger;

        readonly Uri authUrl;

        string _login, _password;

        // TODO: Реализовать блокировку доступа при вызове из нескольких потоков
        /// <summary>
        /// Токен авторизации
        /// </summary>
        private string _authorizationToken = null;
        /// <summary>
        /// Токен для повторного генерирования
        /// </summary>
        private string _refreshToken = null;

        public AuthService(IOptions<TheMapAuthOptions> authOptions
            , ILogger<AuthService> logger)
        {
            _authOptions = authOptions.Value;
            _logger = logger;

            authUrl = new(_authOptions.AuthUrl, "/oauth/token");
        }

        public bool Authentificated { get; private set; }

        public string AccessToken => _authorizationToken;

        public async Task Authentificate(string login, string password, CancellationToken cancellationToken = default)
        {
            _authorizationToken = null;
            _refreshToken = null;
            Authentificated = false;

            _login = login;
            _password = password;

            await GetAuthTokensAsync(cancellationToken);
        }       

        /// <summary>
        /// Получение токена авторизации
        /// </summary>
        /// <param name="cancellationToken">Токен отмены операции</param>
        /// <returns></returns>
        private async Task GetAuthTokensAsync(CancellationToken cancellationToken = default)
        {
            TokenRequest request = new()
            {
                ClientId = _authOptions.ClientId,
                ClientSecret = _authOptions.ClientSecret,
                Username = _login,
                Password = _password
            };

            try
            {
                TokenResponse result = await authUrl
                    .PostJsonAsync(request, cancellationToken)
                    .ReceiveJson<TokenResponse>();

                if (string.IsNullOrEmpty(result.IdToken))
                    throw new Exception("В ответе получено пустое поле 'id_token'");

                if (string.IsNullOrEmpty(result.RefreshToken))
                    throw new Exception("В ответе получено пустое поле 'refresh_token'");

                _authorizationToken = result.IdToken;
                _refreshToken = result.RefreshToken;

                Authentificated = true;
            }
            catch (OperationCanceledException)
            {
                Authentificated = false;
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка в процессе авторизации");
                throw;
            }
        }

        /// <summary>
        /// Обновление IdToken с использованием RefreshToken
        /// </summary>
        /// <param name="cancellationToken">Токен отмены операции</param>
        /// <returns></returns>
        public async Task RefreshIdTokenAsync(CancellationToken cancellationToken = default)
        {
            TokenRequest request = new()
            {
                ClientId = _authOptions.ClientId,
                ClientSecret = _authOptions.ClientSecret,
                GranType = "refresh_token",
                RefreshToken = _refreshToken
            };

            try
            {
                var result = await Policy
                    .HandleResult<TokenResponse>(result => result.Error == RefreshExpiredError)
                    .RetryAsync(1, onRetryAsync: async (exception, retryNumber) => await GetAuthTokensAsync(cancellationToken))
                    .ExecuteAsync(() => authUrl
                        .AllowHttpStatus("403")
                        .PostJsonAsync(request, cancellationToken)
                        .ReceiveJson<TokenResponse>());

                if (string.IsNullOrEmpty(result.IdToken))
                    throw new Exception("В ответе получено пустое поле 'id_token'");

                _authorizationToken = result.IdToken;
                if (!string.IsNullOrEmpty(result.RefreshToken))
                    _refreshToken = result.RefreshToken;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Ошибка обновление токена авторизации с использованием RefreshToken");
                throw;
            }
        }
    }
}
