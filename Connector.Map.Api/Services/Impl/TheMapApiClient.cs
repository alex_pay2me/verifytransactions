﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Connector.Map.Api.Configurations.AppSettings;
using Connector.Map.Api.Models;
using Flurl.Http;
using Flurl.Http.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Polly;

namespace Connector.Map.Api.Services.Impl
{
    /// <summary>
    /// Клиент для работы с Web API TheMAP
    /// </summary>
    internal class TheMapApiClient : ITheMapApiClient
    {
        /// <summary>
        /// Код ошибки выдается при просрочке токена авторизации
        /// </summary>
        const string TokenParseError = "TOKEN_PARSE_ERROR";

        /// <summary>
        /// Код ошибки 
        /// </summary>
        const string AuthenticationFailError = "AUTHENTICATION_FAIL";

        readonly MAPConnectorOptions _options;
        readonly ILogger<TheMapApiClient> _logger;
        readonly IAuthService _authSevice;

        readonly Uri getTransactionsUrl;
        readonly Uri getTransactionTraceUrl;
        readonly Uri getMerchantsUrl;
        readonly Uri getProcessingsUrl;
        readonly Uri getTerminalsUrl;
        readonly Uri getMerchantOrderStateUrl;

        public TheMapApiClient(IAuthService authSevice
            , IOptions<MAPConnectorOptions> options
            , ILogger<TheMapApiClient> logger)
        {
            _authSevice = authSevice;
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            
            getTransactionsUrl = new(_options.ApiUrl, "/api/transaction");
            getTransactionTraceUrl = new(_options.ApiUrl, "/api/trace");
            getProcessingsUrl = new(_options.ApiUrl, "/api/processing");
            getMerchantsUrl = new(_options.ApiUrl, "/api/merchant");
            getTerminalsUrl = new(_options.ApiUrl, "/api/terminal");

            getMerchantOrderStateUrl = new(_options.PaymentApiUrl, "/getState");

            FlurlHttp.Configure(c =>
            {
                c.JsonSerializer = new NewtonsoftJsonSerializer(new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            });
        }

        /// <summary>
        /// Проверить выполнена ли аутентификация
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task EnsureAuthentificated(CancellationToken cancellationToken)
        {
            if (!_authSevice.Authentificated)
            {
                await _authSevice.Authentificate(_options.Username, _options.Password, cancellationToken);
            }
        }

        public async Task<List<Transaction>> GetTransactionsAsync(TransactionsRequest requestParams, CancellationToken cancellationToken = default)
        {
            try
            {
                await EnsureAuthentificated(cancellationToken);

                var result = await Policy
                    .HandleResult<ListResultResponse<Transaction>>(result => result?.ErrorCode == TokenParseError)
                    .RetryAsync(1, onRetryAsync: async (exception, retryNumber) => await _authSevice.RefreshIdTokenAsync(cancellationToken))
                    .ExecuteAsync(() => getTransactionsUrl
                        .AllowHttpStatus("400,403")
                        .WithHeader("Authorization-Token", _authSevice.AccessToken)
                        .PostJsonAsync(requestParams, cancellationToken)
                        .ReceiveJson<ListResultResponse<Transaction>>());

                return result?.ResultItems;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Возникла ошибка при запросе транзакций");
                throw;
            }
        }               
        
        public async Task<List<TransactionTrace>> GetTransactionTraceAsync(TransactionTraceRequest requestParams, CancellationToken cancellationToken = default)
        {
            try
            {
                await EnsureAuthentificated(cancellationToken);

                var result = await Policy
                    .HandleResult<ListResultResponse<TransactionTrace>>(result => result.ErrorCode == TokenParseError)
                    .RetryAsync(1, onRetryAsync: async (exception, retryNumber) => await _authSevice.RefreshIdTokenAsync(cancellationToken))
                    .ExecuteAsync(() => getTransactionTraceUrl
                        .AllowHttpStatus("400,403")
                        .WithHeader("Authorization-Token", _authSevice.AccessToken)
                        .PostJsonAsync(requestParams, cancellationToken)
                        .ReceiveJson<ListResultResponse<TransactionTrace>>());

                if (result.ErrorCode == AuthenticationFailError)
                    throw new Exception("Нет прав на выполнение операции");

                return result.ResultItems;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Возникла ошибка при запросе истории состояний транзакции {TransactionId}", requestParams.TransactionId);
                throw;
            }
        }
                
        public async Task<List<Processing>> GetProcessingsAsync(GetProcessingsRequest requestParams, CancellationToken cancellationToken = default)
        {
            try
            {
                await EnsureAuthentificated(cancellationToken);

                var result = await Policy
                    .HandleResult<ListResultResponse<Processing>>(result => result.ErrorCode == TokenParseError)
                    .RetryAsync(1, onRetryAsync: async (exception, retryNumber) => await _authSevice.RefreshIdTokenAsync(cancellationToken))
                    .ExecuteAsync(() => getProcessingsUrl
                        .AllowHttpStatus("400,403")
                        .WithHeader("Authorization-Token", _authSevice.AccessToken)
                        .PostJsonAsync(requestParams, cancellationToken)
                        .ReceiveJson<ListResultResponse<Processing>>());

                if (result.ErrorCode == AuthenticationFailError)
                    throw new Exception("Нет прав на выполнение операции");

                return result.ResultItems;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Возникла ошибка при запросе списка процессингов");
                throw;
            }
        }
                
        public async Task<List<Merchant>> GetMerchantsAsync(GetMerchantsRequest requestParams, CancellationToken cancellationToken = default)
        {
            try
            {
                await EnsureAuthentificated(cancellationToken);

                var result = await Policy
                    .HandleResult<ListResultResponse<Merchant>>(result => result.ErrorCode == TokenParseError)
                    .RetryAsync(1, onRetryAsync: async (exception, retryNumber) => await _authSevice.RefreshIdTokenAsync(cancellationToken))
                    .ExecuteAsync(() => getMerchantsUrl
                        .AllowHttpStatus("400,403")
                        .WithHeader("Authorization-Token", _authSevice.AccessToken)
                        .PostJsonAsync(requestParams, cancellationToken)
                        .ReceiveJson<ListResultResponse<Merchant>>());

                if (result.ErrorCode == AuthenticationFailError)
                    throw new Exception("Нет прав на выполнение операции");

                return result.ResultItems;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Возникла ошибка при запросе списка предприятий");
                throw;
            }
        }

        public async Task<List<Terminal>> GetTerminalsAsync(GetTerminalsRequest requestParams, CancellationToken cancellationToken = default)
        {
            try
            {
                await EnsureAuthentificated(cancellationToken);

                var result = await Policy
                    .HandleResult<ListResultResponse<Terminal>>(result => result.ErrorCode == TokenParseError)
                    .RetryAsync(1, onRetryAsync: async (exception, retryNumber) => await _authSevice.RefreshIdTokenAsync(cancellationToken))
                    .ExecuteAsync(() => getTerminalsUrl
                        .AllowHttpStatus("400,403")
                        .WithHeader("Authorization-Token", _authSevice.AccessToken)
                        .PostJsonAsync(requestParams, cancellationToken)
                        .ReceiveJson<ListResultResponse<Terminal>>());

                if (result.ErrorCode == AuthenticationFailError)
                    throw new Exception("Нет прав на выполнение операции");

                return result.ResultItems;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Возникла ошибка при запросе списка терминалов");
                throw;
            }
        }

        public async Task<ActualizeOrderResponce> GetMerchantOrderStateAsync(ActualizeOrderRequest requestParams, CancellationToken cancellationToken = default)
        {
            try
            {
                var result = await getMerchantOrderStateUrl
                        .PostJsonAsync(requestParams, cancellationToken)
                        .ReceiveJson<ActualizeOrderResponce>();

                return result;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Возникла ошибка при запросе статуса заказа мерчанта");
                throw;
            }
        }
    }
}
