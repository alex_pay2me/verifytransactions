﻿using Connector.Map.Api.Configurations.AppSettings;
using Connector.Map.Api.Services;
using Connector.Map.Api.Services.Impl;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Connector.Map.Api.IoC
{
    public static class ServiceExtensions
    {
        public static IServiceCollection AddTheMapConnector(this IServiceCollection services, IConfiguration configuration)
        {
            var section = configuration.GetSection(MAPConnectorOptions.Key);
            if (!section.Exists())
                throw new System.Exception($"В конфигурационном файле не найден блок '{MAPConnectorOptions.Key}'");
            services.Configure<MAPConnectorOptions>(section);

            section = configuration.GetSection(TheMapAuthOptions.Key);
            if (!section.Exists())
                throw new System.Exception($"В конфигурационном файле не найден блок '{TheMapAuthOptions.Key}'");
            services.Configure<TheMapAuthOptions>(section);

            services.AddSingleton<IAuthService, AuthService>();
            services.AddSingleton<ITheMapApiClient, TheMapApiClient>();

            return services;
        }
    }
}
