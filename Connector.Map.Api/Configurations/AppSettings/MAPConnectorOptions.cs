﻿using System;

namespace Connector.Map.Api.Configurations.AppSettings
{
    /// <summary>
    /// Настройки сервиса интеграции с API theMAP
    /// </summary>
    public class MAPConnectorOptions
    {
        public const string Key = "MapConnector";        

        /// <summary>
        /// Базовый адрес API theMAP
        /// </summary>
        public Uri ApiUrl { get; set; }

        /// <summary>
        /// Адрес интеграционного API для клиентов TheMAP 
        /// </summary>
        public Uri PaymentApiUrl { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }        
    }
}
