﻿using System;

namespace Connector.Map.Api.Configurations.AppSettings
{
    /// <summary>
    /// Настройки сервиса авторизации
    /// </summary>
    public class TheMapAuthOptions
    {
        public const string Key = "MapAuthOptions";

        /// <summary>
        /// Базовый адрес сервиса
        /// </summary>
        public Uri AuthUrl { get; set; }

        /// <summary>
        /// ClientId
        /// </summary>
        public string ClientId { get; set; }

        /// <summary>
        /// ClientSecret
        /// </summary>
        public string ClientSecret { get; set; }
    }
}
