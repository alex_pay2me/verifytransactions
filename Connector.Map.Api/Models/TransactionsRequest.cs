﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Connector.Map.Api.Models
{
    /// <summary>
    /// Параметры запроса списка транзакций
    /// </summary>
    public class TransactionsRequest
    {
        /// <summary>
        /// Идентификатор транзакции
        /// </summary>
        [Required]
        [JsonProperty("id")]
        public long TransactionId { get; set; } = -1;

        /// <summary>
        /// Смещение выдачи относительно нулевого элемента
        /// </summary>
        [Required]
        [JsonProperty("offset")]
        public int Offset { get; set; } = 0;

        /// <summary>
        /// Запрашиваемое количество транзакций
        /// </summary>
        [Required]
        [JsonProperty("limit")]
        public int Limit { get; set; } = 20;

        /// <summary>
        /// Поле, по которому выполняется сортировка
        /// </summary>
        [Required]
        [JsonProperty("order_by")]
        public string OrderBy { get; set; } = "id";

        /// <summary>
        /// Типа сортировки (asc/desc)
        /// </summary>
        [Required]
        [JsonProperty("order")]
        public string Order { get; set; } = "desc";

        [JsonProperty("order_id_down")]
        public long? OrderIdDown { get; set; }

        /// <summary>
        /// Входящая транзакция (PayIn)
        /// </summary>
        [JsonProperty("is_receipt")]
        public byte? IsReceipt { get; set; }

        /// <summary>
        /// Исходящая транзакция (PayOut)
        /// </summary>
        [JsonProperty("is_payout")]
        public bool? IsPayout { get; set; }

        /// <summary>
        /// Рекуррентные транзакции
        /// </summary>
        [JsonProperty("is_recurrent")]
        public bool? IsRecurrent { get; set; }

        /// <summary>
        /// IPS транзакции
        /// </summary>
        [JsonProperty("is_ips")]
        public bool? IsIPS { get; set; }

        /// <summary>
        /// Диапазон значений суммы 
        /// </summary>
        [JsonProperty("amount_range")]
        public int[] AmountRange { get; set; }

        /// <summary>
        /// Временной интервал. Дата в формате UTC. 
        /// </summary>
        [JsonProperty("date_range")]
        public string[] DateRange { get; set; }

        /// <summary>
        /// Идентификаторы процессингов
        /// </summary>
        [JsonProperty("processings")]
        public int[] Processings { get; set; }

        /// <summary>
        /// Идентификаторы мерчантов
        /// </summary>
        [JsonProperty("merchants")]
        public int[] Merchants { get; set; }

        /// <summary>
        /// Идентификаторы терминалов
        /// </summary>
        [JsonProperty("terminals")]
        public int[] Terminals { get; set; }

        /// <summary>
        /// Держатели карт
        /// </summary>
        [JsonProperty("card_holders")]
        public string[] CardHolders { get; set; }

        /// <summary>
        /// Маски карт
        /// </summary>
        [JsonProperty("pan_masks")]
        public string[] PANMasks { get; set; }

        /// <summary>
        /// RRN
        /// </summary>
        [JsonProperty("rrns")]
        public string[] RRNs { get; set; }

        /// <summary>
        /// Ошибки процессинга
        /// </summary>
        [JsonProperty("processing_errors")]
        public string[] ProcessingErrors { get; set; }

        /// <summary>
        /// Названия банков
        /// </summary>
        [JsonProperty("bank_names")]
        public string[] BankNames { get; set; }

        /// <summary>
        /// Статусы
        /// </summary>
        [JsonProperty("states")]
        public int[] States { get; set; }

        /// <summary>
        /// Статусы в процессинге
        /// </summary>
        [JsonProperty("processing_states")]
        public int[] ProcessingStates { get; set; }

        /// <summary>
        /// Номера заказов мерчанта
        /// </summary>
        [JsonProperty("merchant_orders")]
        public string[] MerchantOrders { get; set; }

        /// <summary>
        /// Номера заказов MAP
        /// </summary>
        [JsonProperty("map_orders")]
        public string[] MAPOrders { get; set; }

        /// <summary>
        /// Значения пользовательских параметров
        /// </summary>
        [JsonProperty("custom_params")]
        public CustomParam[] CustomParams { get; set; }

        /// <summary>
        /// Антифрод решения
        /// </summary>
        [JsonProperty("antifraud_decisions")]
        public int[] AntiFraudDecisions { get; set; }

        /// <summary>
        /// Перечень E-mail адресов
        /// </summary>
        [JsonProperty("emails")]
        public string[] Emails { get; set; }
    }
}
