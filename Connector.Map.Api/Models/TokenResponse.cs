﻿using Newtonsoft.Json;

namespace Connector.Map.Api.Models
{
    /// <summary>
    /// Ответ на запрос токена авторизации
    /// </summary>
    internal class TokenResponse
    {
        /// <summary>
        /// Токен
        /// </summary>
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        /// <summary>
        /// Токен для получения нового <see cref="IdToken"/> по истечению времени жизни
        /// </summary>
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        /// <summary>
        /// Id токен
        /// </summary>
        [JsonProperty("id_token")]
        public string IdToken { get; set; }

        /// <summary>
        /// Время устаревания
        /// </summary>
        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }

        /// <summary>
        /// Тип токена
        /// </summary>
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        /// <summary>
        /// Код ошибки получения токена
        /// </summary>
        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
