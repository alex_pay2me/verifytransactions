﻿using System;
using Newtonsoft.Json;

namespace Connector.Map.Api.Models
{
    /// <summary>
    /// Информация о транзакции 
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// Идентификатор транзакции
        /// </summary>
        [JsonProperty("id")]
        public long Id { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        [JsonProperty("date_created")]
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Дата обновления
        /// </summary>
        [JsonProperty("date_updated")]
        public DateTime DateUpdated { get; set; }

        /// <summary>
        /// Активна
        /// </summary>
        [JsonProperty("active")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Выплата
        /// </summary>
        [JsonProperty("is_payout")]
        public bool IsPayout { get; set; }

        /// <summary>
        /// Рекуррент
        /// </summary>
        [JsonProperty("is_recurrent")]
        public bool IsRecurrent { get; set; }

        /// <summary>
        /// 54-ФЗ
        /// </summary>
        [JsonProperty("is_receipt")]
        public bool IsReceipt { get; set; }

        /// <summary>
        /// IPS
        /// </summary>
        [JsonProperty("is_ips")]
        public bool IsIPS { get; set; }

        /// <summary>
        /// Сплит
        /// </summary>
        [JsonProperty("is_split")]
        public bool IsSplit { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        [JsonProperty("amount")]
        public long Amount { get; set; }

        /// <summary>
        /// Код валюты
        /// </summary>
        [JsonProperty("currency_code")]
        public long CurrencyCode { get; set; }

        /// <summary>
        /// Идентификатор мерчанта
        /// </summary>
        [JsonProperty("merchant_id")]
        public long MerchantID { get; set; }

        /// <summary>
        /// Идентификатор терминала
        /// </summary>
        [JsonProperty("terminal_id")]
        public long TerminalID { get; set; }

        /// <summary>
        /// Номер заказа мерчанта
        /// </summary>
        [JsonProperty("merchant_order_id")]
        public string MerchantOrderID { get; set; }

        /// <summary>
        /// Номер заказа MAP
        /// </summary>
        [JsonProperty("map_order_id")]
        public string MAPOrderID { get; set; }

        /// <summary>
        /// Номер заказа процессинга
        /// </summary>
        [JsonProperty("processing_order_id")]
        public string ProcessingOrderID { get; set; }

        /// <summary>
        /// Идентификатор процессинга
        /// </summary>
        [JsonProperty("processing_id")]
        public long ProcessingID { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        [JsonProperty("state_id")]
        public int StateID { get; set; }

        /// <summary>
        /// Статус в процессинге
        /// </summary>
        [JsonProperty("processing_state_id")]
        public int ProcessingStateID { get; set; }

        /// <summary>
        /// Код ошибки в процессинге
        /// </summary>
        [JsonProperty("processing_error_code")]
        public string ProcessingErrorCode { get; set; }

        /// <summary>
        /// Ответ эквайера
        /// </summary>
        [JsonProperty("processing_message")]
        public string ProcessingMessage { get; set; }

        /// <summary>
        /// Маска карты
        /// </summary>
        [JsonProperty("pan_mask")]
        public string PANMask { get; set; }

        /// <summary>
        /// Держатель карты
        /// </summary>
        [JsonProperty("card_holder")]
        public string CardHolder { get; set; }

        /// <summary>
        /// IP адрес
        /// </summary>
        [JsonProperty("ip")]
        public string IP { get; set; }

        /// <summary>
        /// Код страны
        /// </summary>
        [JsonProperty("ip_country_code")]
        public string IPCountryCode { get; set; }

        /// <summary>
        /// Идентификатор типа карты
        /// </summary>
        [JsonProperty("card_type_id")]
        public long CardTypeID { get; set; }

        /// <summary>
        /// Код типа карты
        /// </summary>
        [JsonProperty("card_type_code")]
        public string CardTypeCode { get; set; }

        /// <summary>
        /// ПС карты
        /// </summary>
        [JsonProperty("card_type_name")]
        public string CardTypeName { get; set; }

        /// <summary>
        /// Тип карты
        /// </summary>
        [JsonProperty("card_type_type")]
        public string CardTypeType { get; set; }

        /// <summary>
        /// Тип продукта
        /// </summary>
        [JsonProperty("card_product_type")]
        public string CardProductType { get; set; }

        /// <summary>
        /// Банк
        /// </summary>
        [JsonProperty("bank_name")]
        public string BankName { get; set; }

        /// <summary>
        /// Город банка
        /// </summary>
        [JsonProperty("bank_city")]
        public string BankCity { get; set; }

        /// <summary>
        /// Код страны банка
        /// </summary>
        [JsonProperty("bank_country_code")]
        public string BankCountryCode { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Сплит
        /// </summary>
        [JsonProperty("no_cvv")]
        public bool NoCVV { get; set; }

        /// <summary>
        /// RRN
        /// </summary>
        [JsonProperty("rrn")]
        public string RRN { get; set; }

        /// <summary>
        /// Код авторизации
        /// </summary>
        [JsonProperty("auth_code")]
        public string AuthCode { get; set; }
    }
}
