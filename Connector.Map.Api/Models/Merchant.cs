﻿using Newtonsoft.Json;

namespace Connector.Map.Api.Models
{
    /// <summary>
    /// Класс, описывающий предприятие
    /// </summary>
    public class Merchant
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [JsonProperty("id")]
        public long Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        [JsonProperty("human_name")]
        public string HumanName { get; set; }
    }
}
