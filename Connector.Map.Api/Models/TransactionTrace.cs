﻿using System;
using Newtonsoft.Json;

namespace Connector.Map.Api.Models
{
    /// <summary>
    /// Информация о состоянии транзакции 
    /// </summary>
    public class TransactionTrace
    {
        /// <summary>
        /// Идентификатор состояния транзакции
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Метка времени создания
        /// </summary>
        [JsonProperty("date_created")]
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Метка времени изменения
        /// </summary>
        [JsonProperty("date_updated")]
        public DateTime DateUpdated { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        [JsonProperty("amount")]
        public long Amount { get; set; }

        /// <summary>
        /// Код валюты
        /// </summary>
        [JsonProperty("currency_code")]
        public long CurrencyCode { get; set; }

        /// <summary>
        /// Идентификатор транзакции
        /// </summary>
        [JsonProperty("transaction_id")]
        public long TransactionID { get; set; }

        /// <summary>
        /// Статус 
        /// </summary>
        [JsonProperty("state_id")]
        public int StateID { get; set; }

        /// <summary>
        /// Статус в процессинге
        /// </summary>
        [JsonProperty("processing_state_id")]
        public int ProcessingStateID { get; set; }

        /// <summary>
        /// Код ошибки в процессинге
        /// </summary>
        [JsonProperty("processing_error_code")]
        public string ProcessingErrorCode { get; set; }

        /// <summary>
        /// Кем создана
        /// </summary>
        [JsonProperty("created_by")]
        public string CreatedBy { get; set; }
    }
}
