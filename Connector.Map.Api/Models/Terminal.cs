﻿using Newtonsoft.Json;

namespace Connector.Map.Api.Models
{
    /// <summary>
    /// Класс, описывающий предприятие
    /// </summary>
    public class Terminal
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [JsonProperty("id")]
        public long Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Ид. процессинга (Банка)
        /// </summary>
        [JsonProperty("processing_id")]
        public long ProcessingId { get; set; }

        /// <summary>
        /// Ид. мерчанта (Предприятия)
        /// </summary>
        [JsonProperty("merchant_id")]
        public long MerchantId { get; set; }
    }
}
