﻿namespace Connector.Map.Api.Models
{
    public class ActualizeOrderResponce
    {
        /// <summary>
        /// Статус выполнения операции
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Ид. заказа
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// Статус 
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Код авторизации
        /// </summary>
        public string AuthCode { get; set; }

        /// <summary>
        /// Тип карты
        /// </summary>
        public string CardType { get; set; }

        /// <summary>
        /// Идентификатор заказа у мерчанта
        /// </summary>
        public string MerchantOrderId { get; set; }

        /// <summary>
        /// Reference Retrieval Number
        /// </summary>
        public string RRN { get; set; }
    }
}
