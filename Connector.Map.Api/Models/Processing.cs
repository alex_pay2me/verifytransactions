﻿using System;
using Newtonsoft.Json;

namespace Connector.Map.Api.Models
{
    /// <summary>
    /// Класс, описывающий предприятие
    /// </summary>
    public class Processing
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [JsonProperty("id")]
        public long Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        [JsonProperty("human_name")]
        public string HumanName { get; set; }

        /// <summary>
        /// Ссылку на иконку процессинга
        /// </summary>
        [JsonProperty("icon_url")]
        public Uri IconUrl { get; set; }
    }
}
