﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Connector.Map.Api.Models
{
    /// <summary>
    /// Ответ на запрос с результатом в виде списка сущностей
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal class ListResultResponse<T>
    {
        /// <summary>
        /// Код ошибки
        /// </summary>
        [JsonProperty("error_code")]
        public string ErrorCode { get; set; }

        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }

        /// <summary>
        /// Код состояния
        /// </summary>
        [JsonProperty("state_id")]
        public int StateId { get; set; }

        /// <summary>
        /// Количество записей
        /// </summary>
        [JsonProperty("total")]
        public int Count { get; set; }

        /// <summary>
        /// Список с результатом запроса
        /// </summary>
        [JsonProperty("list")]
        public List<T> ResultItems { get; set; }
    }
}
