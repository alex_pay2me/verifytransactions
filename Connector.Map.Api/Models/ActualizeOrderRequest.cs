﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Connector.Map.Api.Models
{
    /// <summary>
    /// Параметры запроса актуализации статуса заказа у мерчанта
    /// </summary>
    /// <remarks>В запросе должен быть задан только один из параметров <see cref="ActualizeOrderRequest.MapOrderId"/> <see cref="ActualizeOrderRequest.MerchantOrderId"/></remarks>
    public class ActualizeOrderRequest
    {
        /// <summary>
        /// Название терминала, <see cref="Terminal.Name"/>
        /// </summary>
        [JsonProperty("key")]
        [Required]
        public string Key { get; set; }

        /// <summary>
        /// Идентификатор платежа в системе theMAP
        /// </summary>
        [JsonProperty("map_order_id")]
        [Required]
        public string MapOrderId { get; set; }

        /// <summary>
        /// Идентификатор платежа в системе Продавца (Мерчанта)
        /// </summary>
        [JsonProperty("merchant_order_id")]
        [Required]
        public string MerchantOrderId { get; set; }

        /// <summary>
        /// Выполнить актуализацию статуса заказа
        /// </summary>
        [JsonProperty("is_overload")]
        [Required]
        public bool IsOverload { get; set; }
    }
}
