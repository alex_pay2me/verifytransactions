﻿using Newtonsoft.Json;

namespace Connector.Map.Api.Models
{
    /// <summary>
    /// Запрос токена авторизации
    /// </summary>
    internal class TokenRequest
    {
        [JsonProperty("client_id")]
        public string ClientId { get; set; }

        [JsonProperty("client_secret")]
        public string ClientSecret { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("grant_type")]
        public string GranType { get; set; } = "password";

        [JsonProperty("scope")]
        public string Scope { get; set; } = "offline_access";
    }
}
