﻿using Newtonsoft.Json;

namespace Connector.Map.Api.Models
{
    /// <summary>
    /// Пользовательский параметр
    /// </summary>
    public class CustomParam
    {
        /// <summary>
        /// Ключ
        /// </summary>
        [JsonProperty("k")]
        public string Key { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        [JsonProperty("v")]
        public string Value { get; set; }
    }
}
