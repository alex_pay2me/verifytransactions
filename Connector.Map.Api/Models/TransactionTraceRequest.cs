﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Connector.Map.Api.Models
{
    /// <summary>
    /// Данные запроса состояний транзакции
    /// </summary>
    public class TransactionTraceRequest
    {
        /// <summary>
        /// Идентификатор транзакции
        /// </summary>
        [Required]
        [JsonProperty("transaction_id")]
        public long TransactionId { get; set; } = -1;

        /// <summary>
        /// Смещение выдачи относительно нулевого элемента
        /// </summary>
        [Required]
        [JsonProperty("offset")]
        public int Offset { get; set; } = 0;

        /// <summary>
        /// Поле, по которому выполняется сортировка
        /// </summary>
        [Required]
        [JsonProperty("order_by")]
        public string OrderBy { get; set; } = "id";

        /// <summary>
        /// Типа сортировки (asc/desc)
        /// </summary>
        [Required]
        [JsonProperty("order")]
        public string Order { get; set; } = "desc";
    }
}
